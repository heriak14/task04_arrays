package com.epam.arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ArrayPrinter {
    private static final Logger LOG = LogManager.getLogger();

    public static void testUtils() {
        int[] array1 = {1, 3, 5, 4, 4, 4, 6, 2, 2, 3, 4};
        int[] array2 = {4, 3, 1, 9, 9, 9, 9, 0, 8, 8};
        LOG.trace("Given two arrays:\nArray1: " + Arrays.toString(array1)
                + "\nArray2: " + Arrays.toString(array2) + "\n");
        LOG.trace("Common elements: " + Arrays.toString(ArrayUtils.findCommonElements(array1, array2)) + "\n");
        LOG.trace("Different elements: " + Arrays.toString(ArrayUtils.findDifferentElements(array1, array2)) + "\n");
        LOG.trace("Array1 without duplicates: " + Arrays.toString(ArrayUtils.deleteDuplicates(array1)) + "\n");
        LOG.trace("Array2 without sequences of elements: " + Arrays.toString(ArrayUtils.deleteSequences(array2)) + "\n\n");
    }
}

package com.epam.arrays;

import java.util.Arrays;
import java.util.Objects;

public class ArrayUtils {

    public static int[] findCommonElements(int[] array1, int[] array2) { ;
        Integer[] comm = new Integer[array1.length];
        for (int i = 0; i < array1.length; i++) {
            if (contains(array2, array1[i])) {
                comm[i] = array1[i];
            } else {
                comm[i] = null;
            }
        }
        return trimToSize(comm);
    }

    public static int[] findDifferentElements(int[] array1, int[] array2) {
        Integer[] diff = new Integer[array1.length];
        for (int i = 0; i < array1.length; i++) {
            if (!contains(array2, array1[i])) {
                diff[i] = array1[i];
            } else {
                diff[i] = null;
            }
        }
        return trimToSize(diff);
    }

    public static int[] deleteDuplicates(int[] array) {
        Integer[] res = new Integer[array.length];
        for (int i = 0; i < array.length; i++) {
            if (numOfOccur(array, array[i]) < 2) {
                res[i] = array[i];
            } else {
                res[i] = null;
            }
        }
        return trimToSize(res);
    }

    public static int[] deleteSequences(int[] array) {
        Integer[] res = new Integer[array.length];
        for (int i = 0; i < array.length; i++) {
            if (i < array.length - 1 && array[i] == array[i + 1]) {
                res[i] = null;
            } else {
                res[i] = array[i];
            }
        }
        return trimToSize(res);
    }

    public static boolean contains(int[] array, int el) {
        for (int e : array) {
            if (e == el) {
                return true;
            }
        }
        return false;
    }

    public static boolean contains(Integer[] array, int el) {
        for (int e : array) {
            if (e == el) {
                return true;
            }
        }
        return false;
    }

    private static int numOfOccur(int[] array, int el) {
        int count = 0;
        for (int e : array) {
            if (e == el) {
                count++;
            }
        }
        return count;
    }

    private static int[] trimToSize(Integer[] array) {
        int[] res;
        int resSize = 0;
        for (Integer i : array) {
            if (!Objects.isNull(i)) {
                resSize++;
            }
        }
        res = new int[resSize];
        int i = 0;
        for (Integer e : array) {
            if (!Objects.isNull(e)) {
                res[i] = e;
                i++;
            }
        }
        return res;
    }
}

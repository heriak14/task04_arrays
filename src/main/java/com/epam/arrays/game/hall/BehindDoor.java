package com.epam.arrays.game.hall;

import com.epam.property.Property;

public enum BehindDoor {
    ARTIFACT(
            Integer.parseInt(Property.GAME.getProperty("artifact.power.min")),
            Integer.parseInt(Property.GAME.getProperty("artifact.power.max"))
    ), MONSTER(
            Integer.parseInt(Property.GAME.getProperty("monster.power.min")),
            Integer.parseInt(Property.GAME.getProperty("monster.power.max"))
    );

    private final int minPower;
    private final int maxPower;

    BehindDoor(int minPower, int maxPower) {
        this.minPower = minPower;
        this.maxPower = maxPower;
    }

    public int getMinPower() {
        return minPower;
    }

    public int getMaxPower() {
        return maxPower;
    }
}

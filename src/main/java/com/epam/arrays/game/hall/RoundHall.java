package com.epam.arrays.game.hall;

import com.epam.property.Property;

import java.util.Random;

public class RoundHall {
    private static final int NUM_OF_DOORS = Integer.parseInt(Property.GAME.getProperty("hall.door.number"));
    private static final Random RAND = new Random();
    private Door[] doors;

    public RoundHall() {
        doors = new Door[NUM_OF_DOORS];
        for (int i = 0; i < doors.length; i++) {
            doors[i] = new Door();
            doors[i].setBehindDoor(BehindDoor.values()[RAND.nextInt(BehindDoor.values().length)]);
            doors[i].setPower(RAND.nextInt(doors[i].getBehindDoor().getMaxPower()
                    - doors[i].getBehindDoor().getMinPower() + 1)
                    + doors[i].getBehindDoor().getMinPower());
        }
    }

    public Door[] getDoors() {
        return doors;
    }
}

package com.epam.arrays.game.hall;

public class Door {
    private BehindDoor behindDoor;
    private int power;

    public Door() {
    }

    public Door(BehindDoor behindDoor, int power) {
        this.behindDoor = behindDoor;
        this.power = power;
    }

    public BehindDoor getBehindDoor() {
        return behindDoor;
    }

    public void setBehindDoor(BehindDoor behindDoor) {
        this.behindDoor = behindDoor;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

}

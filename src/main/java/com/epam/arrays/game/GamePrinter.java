package com.epam.arrays.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

public class GamePrinter {
    private static final Logger LOG = LogManager.getLogger();
    private Player player;

    public GamePrinter() {
        player = new Player();
    }

    public void showGame() {
        LOG.trace(player.getDoorTable());
        LOG.trace("Behind " + player.countDeathDoor() + " doors death is waiting for you!\n");
        LOG.trace(player.hasChance() ? "You have a chance!\n" : "You don't have a chance!\n");
        int[] winComb = player.getWinCombination();
        if (!Objects.isNull(winComb)) {
            LOG.trace("Just follow this door combination: ");
            for (int i : winComb) {
                LOG.trace(i + " ");
            }
        }
    }
}

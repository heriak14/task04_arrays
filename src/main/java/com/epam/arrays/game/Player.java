package com.epam.arrays.game;

import com.epam.arrays.ArrayUtils;
import com.epam.arrays.game.hall.BehindDoor;
import com.epam.arrays.game.hall.Door;
import com.epam.arrays.game.hall.RoundHall;
import com.epam.property.Property;


public class Player {

    private static final int START_POWER = Integer.parseInt(Property.GAME.getProperty("player.power.start"));
    private int playerPower;
    private RoundHall hall;

    public Player() {
        playerPower = START_POWER;
        hall = new RoundHall();
    }

    public String getDoorTable() {
        String table = "\t\t\t\tDOORS\n";
        table += "-------------------------------------\n";
        table += "|\t№\t|\tBehind\t\t|\tPower\t|\n";
        table += "-------------------------------------\n";
        for (int i = 0; i < hall.getDoors().length; i++) {
            table += "|\t" + (i + 1) + "\t|\t" + hall.getDoors()[i].getBehindDoor()
                    + " \t|\t" + hall.getDoors()[i].getPower() + "\t\t|\n";
            table += "-------------------------------------\n";
        }
        return table;
    }

    public int countDeathDoor() {
        return countDeathDoor(0, hall.getDoors());
    }

    private int countDeathDoor(int door, Door[] doors) {
        if (door == doors.length - 1) {
            return isDeath(door, doors);
        }
        return isDeath(door, doors) + countDeathDoor(door + 1, doors);
    }

    private int isDeath(int num, Door[] doors) {
        if ((doors[num].getBehindDoor() == BehindDoor.MONSTER)
                && (doors[num].getPower() > playerPower)) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean hasChance() {
        int power = playerPower;
        for (int i = 0; i < hall.getDoors().length; i++) {
            if (hall.getDoors()[i].getBehindDoor() == BehindDoor.MONSTER) {
                power -= hall.getDoors()[i].getPower();
            } else {
                power += hall.getDoors()[i].getPower();
            }
        }
        return (power >= 0);
    }

    public int[] getWinCombination() {
        if (!hasChance()) {
            return null;
        }
        int[] winDoors = new int[hall.getDoors().length];

        for (int i = 0, k = 0; i < hall.getDoors().length; i++) {
            for (int j = 0; j < hall.getDoors().length; j++) {
                Door d = hall.getDoors()[j];
                if (isDeath(j, hall.getDoors()) == 1) {
                    continue;
                }
                if (ArrayUtils.contains(winDoors, j + 1)) {
                    continue;
                }
                if (d.getBehindDoor() == BehindDoor.MONSTER) {
                    playerPower -= d.getPower();
                } else {
                    playerPower += d.getPower();
                }
                winDoors[k] = j + 1;
                k++;
            }
        }
        return winDoors;
    }
}

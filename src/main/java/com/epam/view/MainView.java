package com.epam.view;

import com.epam.arrays.ArrayPrinter;
import com.epam.arrays.game.GamePrinter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {

    private static final Logger LOG = LogManager.getLogger();
    private static final Scanner SCAN = new Scanner(System.in, "UTF-8");
    private Map<String, String> mainMenu;
    private Map<String, Runner> runMenu;

    public MainView() {
        mainMenu = new LinkedHashMap<>();
        mainMenu.put("1", "TEST ARRAY TASKS");
        mainMenu.put("2", "START ARRAY-GAME");

        runMenu = new LinkedHashMap<>();
        runMenu.put("1", this::testArrays);
        runMenu.put("2", this::startGame);
    }

    private void showMenu() {
        for (String key : mainMenu.keySet()) {
            LOG.trace(key + " - " + mainMenu.get(key) + "\n");
        }
    }

    private void startGame() {
        new GamePrinter().showGame();
    }

    private void testArrays() {
        ArrayPrinter.testUtils();
    }

    public void show() {
        String key;
        do {
            showMenu();
            LOG.trace("Enter what you want to see (q - quit): ");
            key = SCAN.nextLine();
            try {
                runMenu.get(key).run();
            } catch (NullPointerException e) {
                LOG.trace("Wrong input!\n");
            }
        } while (!key.equals("q"));
    }
}
